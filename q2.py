import numpy as np
import keras, sys, os
from keras import backend as K
from keras import callbacks, regularizers, optimizers, utils, losses
from keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense, Activation
from keras.layers.core import Reshape
from keras.models import Model
from sklearn.datasets import fetch_mldata
import matplotlib.pyplot as plt
import tensorflow as tf

from keras.engine.topology import Layer
from keras.legacy import interfaces

# def identical_shape(input_shape):
#     return input_shape

def NegCrossEntropy(yt, yp):
    return tf.multiply(losses.categorical_crossentropy(yt, yp),1.0)

# class Optimizer(object):
#     """Abstract optimizer base class.
#     Note: this is the parent class of all optimizers, not an actual optimizer
#     that can be used for training models.
#     All Keras optimizers support the following keyword arguments:
#         clipnorm: float >= 0. Gradients will be clipped
#             when their L2 norm exceeds this value.
#         clipvalue: float >= 0. Gradients will be clipped
#             when their absolute value exceeds this value.
#     """
#
#     def __init__(self, **kwargs):
#         allowed_kwargs = {'clipnorm', 'clipvalue'}
#         for k in kwargs:
#             if k not in allowed_kwargs:
#                 raise TypeError('Unexpected keyword argument '
#                                 'passed to optimizer: ' + str(k))
#         self.__dict__.update(kwargs)
#         self.updates = []
#         self.weights = []
#
#     @interfaces.legacy_get_updates_support
#     def get_updates(self, loss, params):
#         raise NotImplementedError
#
#     def get_gradients(self, loss, params):
#         grads = K.gradients(loss, params)
#         if hasattr(self, 'clipnorm') and self.clipnorm > 0:
#             norm = K.sqrt(sum([K.sum(K.square(g)) for g in grads]))
#             grads = [clip_norm(g, self.clipnorm, norm) for g in grads]
#         if hasattr(self, 'clipvalue') and self.clipvalue > 0:
#             grads = [K.clip(g, -self.clipvalue, self.clipvalue) for g in grads]
#         return grads
#
#     def set_weights(self, weights):
#         """Sets the weights of the optimizer, from Numpy arrays.
#         Should only be called after computing the gradients
#         (otherwise the optimizer has no weights).
#         # Arguments
#             weights: a list of Numpy arrays. The number
#                 of arrays and their shape must match
#                 number of the dimensions of the weights
#                 of the optimizer (i.e. it should match the
#                 output of `get_weights`).
#         # Raises
#             ValueError: in case of incompatible weight shapes.
#         """
#         params = self.weights
#         weight_value_tuples = []
#         param_values = K.batch_get_value(params)
#         for pv, p, w in zip(param_values, params, weights):
#             if pv.shape != w.shape:
#                 raise ValueError('Optimizer weight shape ' +
#                                  str(pv.shape) +
#                                  ' not compatible with '
#                                  'provided weight shape ' + str(w.shape))
#             weight_value_tuples.append((p, w))
#         K.batch_set_value(weight_value_tuples)
#
#     def get_weights(self):
#         """Returns the current value of the weights of the optimizer.
#         # Returns
#             A list of numpy arrays.
#         """
#         return K.batch_get_value(self.weights)
#
#     def get_config(self):
#         config = {}
#         if hasattr(self, 'clipnorm'):
#             config['clipnorm'] = self.clipnorm
#         if hasattr(self, 'clipvalue'):
#             config['clipvalue'] = self.clipvalue
#         return config
#
#     @classmethod
#     def from_config(cls, config):
#         return cls(**config)

class my_SGD(optimizers.Optimizer):
    """Stochastic gradient descent optimizer.
    Includes support for momentum,
    learning rate decay, and Nesterov momentum.
    # Arguments
        lr: float >= 0. Learning rate.
        momentum: float >= 0. Parameter updates momentum.
        decay: float >= 0. Learning rate decay over each update.
        nesterov: boolean. Whether to apply Nesterov momentum.
    """

    def __init__(self, lr=0.01, momentum=0., decay=0.,
                 nesterov=False, **kwargs):
        super(my_SGD, self).__init__(**kwargs)
        with K.name_scope(self.__class__.__name__):
            self.iterations = K.variable(0, dtype='int64', name='iterations')
            self.lr = K.variable(lr, name='lr')
            self.momentum = K.variable(momentum, name='momentum')
            self.decay = K.variable(decay, name='decay')
        self.initial_decay = decay
        self.nesterov = nesterov

    @interfaces.legacy_get_updates_support
    def get_updates(self, loss, params):
        grads = self.get_gradients(loss, params)
        self.updates = [K.update_add(self.iterations, 1)]

        lr = self.lr
        if self.initial_decay > 0:
            lr *= (1. / (1. + self.decay * K.cast(self.iterations,
                                                  K.dtype(self.decay))))
        # momentum
        shapes = [K.int_shape(p) for p in params]
        moments = [K.zeros(shape) for shape in shapes]
        self.weights = [self.iterations] + moments
        for p, g, m in zip(params, grads, moments):
            v = self.momentum * m - lr * K.sign(g)  # velocity
            self.updates.append(K.update(m, v))

            if self.nesterov:
                new_p = p + self.momentum * v - lr * K.sign(g)
            else:
                new_p = p + v

            # Apply constraints.
            if getattr(p, 'constraint', None) is not None:
                new_p = p.constraint(new_p)

            self.updates.append(K.update(p, new_p))
        return self.updates

    def get_config(self):
        config = {'lr': float(K.get_value(self.lr)),
                  'momentum': float(K.get_value(self.momentum)),
                  'decay': float(K.get_value(self.decay)),
                  'nesterov': self.nesterov}
        base_config = super(my_SGD, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

class AddNoise(Layer):
    def __init__(self, **kwargs):
        super(AddNoise, self).__init__(**kwargs)

    def build(self, input_shape):
        self.kernel = self.add_weight(name = 'kernel',
        shape = (input_shape[1],input_shape[2],input_shape[3]),
        initializer = keras.initializers.zeros(),
        trainable = True)
        super(AddNoise, self).build(input_shape)

    def call(self, x):
        return tf.clip_by_value(tf.add(x, self.kernel),0,255)

    def compute_output_shape(self, input_shape):
        return input_shape

class TrainHistory(keras.callbacks.Callback):
    def __init__(self, X_val, Y_val):
        self.x_val = X_val
        self.y_val = Y_val
        self.train_losses = []
        self.train_accuracies = []
        self.val_losses = []
        self.val_accuracies = []
        self.count = 0

        # self.temp = []

    def on_batch_end(self, batch, logs={}):
        self.train_losses.append(logs.get('loss'))
        self.train_accuracies.append(logs.get('acc'))

        # ensure noise is between 0 and 255
        # intermediate_layer_model1 = Model(inputs = self.model.input,outputs = self.model.layers[1].output)
        # intermediate_output1 = intermediate_layer_model1.predict(self.x_val[0].reshape((1,28,28,1)))
        # noise = self.model.layers[1].get_weights()
        # subtrahend = np.clip(noise,0,255)
        # self.model.layers[1].set_weights(np.clip(noise,0,255))

        if self.count%200 == 0:
            val_score = self.model.evaluate(self.x_val, self.y_val, batch_size = 100, verbose = 0)
            self.val_losses.append(val_score[0])
            self.val_accuracies.append(val_score[1])

        # if self.count%100 == 0:
        #     print('Weights: ')
        #     print(np.array(self.model.layers[1].get_weights()).flatten()[0:100])
        #     print('Layer 0:')
        #     intermediate_layer_model = Model(inputs = self.model.input,outputs = self.model.layers[0].output)
        #     intermediate_output = intermediate_layer_model.predict(self.x_val[0].reshape((1,28,28,1)))
        #     print(np.array(intermediate_output).flatten()[0:100])
        #     print('Layer 1:')
        #     intermediate_layer_model1 = Model(inputs = self.model.input,outputs = self.model.layers[1].output)
        #     intermediate_output1 = intermediate_layer_model1.predict(self.x_val[0].reshape((1,28,28,1)))
        #     print(np.array(intermediate_output1).flatten()[0:100])
        #     print('Out:')
        #     intermediate_layer_model2 = Model(inputs = self.model.input,outputs = self.model.layers[10].output)
        #     intermediate_output2 = intermediate_layer_model2.predict(self.x_val[0].reshape((1,28,28,1)))
        #     print(np.array(intermediate_output2).flatten()[0:100])

            # if len(self.temp) == 0:
            #     self.temp.extend(self.model.layers[4].get_weights())
            # else:
            #     diff = np.array(self.temp) - np.array(self.model.layers[4].get_weights())
            #     self.temp = []
            #     self.temp.extend(self.model.layers[4].get_weights())
            #     print("Diff : " + str(np.sum(np.sum(diff.flatten()))))


            # noise = self.model.layers[1].get_weights()
            # print(np.clip(noise,0,255).flatten())

        self.count += 1

    def on_train_end(self, logs={}):
        val_score = self.model.evaluate(self.x_val, self.y_val, batch_size = 100, verbose = 0)
        self.val_losses.append(val_score[0])
        self.val_accuracies.append(val_score[1])

class mnist_adversarial_CNN:
    def __init__(self, target_class):
        self.BATCH_SIZE = 100
        self.INIT_STDDEV = 0.08
        self.REGULARIZATION = 0.005
        self.LR = 0.7
        self.DECAY = 0.0001
        self.EPOCHS = 3
        self.MOMENTUM = 0
        self.tc = target_class

        self.load_data()
        self.CNN = self.get_frozen_CNN()
        self.history = TrainHistory(self.x_val, self.y_val)
        self.save_file = str.format('mnist_adv_cnn_{0}',str(self.tc))

        if os.path.exists(self.save_file + '.hdf5'):
            self.CNN.load_weights(self.save_file + '.hdf5')

    def load_data(self):
        mnist = fetch_mldata('MNIST original', data_home='./Data/')
        rand_ind = np.random.permutation(range(70000))
        data = mnist.data[rand_ind]
        labels = mnist.target[rand_ind]

        # original: actual class, the other 'y' values are created to match the target class
        self.x_train = mnist.data[rand_ind[0:50000]].reshape((-1,28,28,1))
        # self.single = mnist.data[25000]
        # temp = np.repeat(self.single,50000,axis = 0).reshape((784,50000))
        # self.x_train = np.transpose(temp).reshape((-1,28,28,1))
        self.y_train_orig = utils.to_categorical(mnist.target[rand_ind[0:50000]], 10)
        self.y_train = utils.to_categorical(self.tc * np.ones(50000),10)

        self.x_val = mnist.data[rand_ind[50000:60000]].reshape((-1,28,28,1))
        self.y_val_orig = utils.to_categorical(mnist.target[rand_ind[50000:60000]], 10)
        self.y_val = utils.to_categorical(self.tc * np.ones(10000),10)

        self.x_test = mnist.data[rand_ind[60000:70000]].reshape((-1,28,28,1))
        self.y_test_orig = utils.to_categorical(mnist.target[rand_ind[60000:70000]], 10)
        self.y_test = utils.to_categorical(self.tc * np.ones(10000),10)

    def get_conv(self, layer_obj):
        return Conv2D(32,(3,3), activation = 'relu', padding = 'same',
        kernel_initializer = 'glorot_normal',
        bias_initializer = keras.initializers.RandomNormal(mean = 1.0, stddev=self.INIT_STDDEV),
        kernel_regularizer = regularizers.l2(self.REGULARIZATION))(layer_obj)

    def get_frozen_conv(self, layer_obj):
        return Conv2D(32,(3,3), activation = 'relu', padding = 'same',
        trainable = False)(layer_obj)

    def get_dense(self, layer_obj, num_units):
        return Dense(num_units,
        kernel_initializer = 'glorot_normal',
        bias_initializer = keras.initializers.RandomNormal(mean = 1.0, stddev=self.INIT_STDDEV),
        kernel_regularizer = regularizers.l2(self.REGULARIZATION))(layer_obj)

    def get_frozen_dense(self, layer_obj, num_units):
        return Dense(num_units, trainable = False)(layer_obj)

    def get_pretrained_weights(self):
        inputs = Input(shape = (28,28,1))
        conv1 = self.get_conv(inputs)
        pool1 = MaxPooling2D(pool_size = (2,2))(conv1)
        conv2 = self.get_conv(pool1)
        pool2 = MaxPooling2D(pool_size = (2,2))(conv2)
        dense1 = self.get_dense(Flatten()(pool2),500)
        dense1 = Activation('relu')(dense1)
        out = self.get_dense(dense1,10)
        out = Activation('softmax')(out)
        model = Model(input = inputs, output = out)
        model.compile(optimizer = optimizers.SGD(lr = self.LR, momentum = self.MOMENTUM, decay = self.DECAY),
        loss = 'categorical_crossentropy', metrics = ['accuracy'])
        model.load_weights('mnist_cnn.hdf5')

        # layers : Input, Conv[1], Pool, Conv[3], Pool, Flatten, Dense[6], Relu, Dense[8], Softmax

        return {'conv1': model.layers[1].get_weights(),
        'conv2': model.layers[3].get_weights(),
        'dense1': model.layers[6].get_weights(),
        'out': model.layers[8].get_weights()}

    def get_frozen_CNN(self):
        pt_weights = self.get_pretrained_weights()

        inputs = Input(shape = (28,28,1))
        input_noise = AddNoise()(inputs)

        conv1 = self.get_frozen_conv(input_noise)
        pool1 = MaxPooling2D(pool_size = (2,2))(conv1)

        conv2 = self.get_frozen_conv(pool1)
        pool2 = MaxPooling2D(pool_size = (2,2))(conv2)

        dense1 = self.get_frozen_dense(Flatten()(pool2),500)
        dense1 = Activation('relu')(dense1)

        out = self.get_frozen_dense(dense1,10)
        out = Activation('softmax')(out)

        model = Model(input = inputs, output = out)

        # in this case, layers : inputs, noise[1], conv[2], pool, conv[4], pool, flatten, dense[7], relu, dense[9], softmax
        model.layers[2].set_weights(pt_weights['conv1'])
        model.layers[4].set_weights(pt_weights['conv2'])
        model.layers[7].set_weights(pt_weights['dense1'])
        model.layers[9].set_weights(pt_weights['out'])
        # lr = self.LR, momentum = self.MOMENTUM, decay = self.DECAY
        self.m_sgd = my_SGD(lr = self.LR, momentum = self.MOMENTUM, decay = self.DECAY)
        model.compile(loss = 'categorical_crossentropy',
        optimizer = my_SGD(lr = self.LR, momentum = self.MOMENTUM, decay = self.DECAY),
         metrics = ['accuracy'])
        # for ml in model.layers:
        #     ml.trainable = False
        # model.layers[1].trainable = True
        return model

    def train(self):
        hist = self.CNN.fit(self.x_train, self.y_train, batch_size = self.BATCH_SIZE, epochs = self.EPOCHS, verbose = 1, shuffle = True,
        callbacks = [self.history])

    def train_no_history(self):
        self.CNN.fit(self.x_train, self.y_train, batch_size = self.BATCH_SIZE, epochs = self.EPOCHS, verbose = 1, shuffle = True)
        # self.CNN.save(self.save_file + '.hdf5')

if __name__ == '__main__':
    target = 0
    if len(sys.argv) > 1:
        target = int(sys.argv[1])
    mycnn = mnist_adversarial_CNN(target)
    mycnn.train()

    np.save(mycnn.save_file + '_train_losses.npy', mycnn.history.train_losses)
    np.save(mycnn.save_file + '_train_accuracy.npy', mycnn.history.train_accuracies)
    np.save(mycnn.save_file + '_val_losses.npy', mycnn.history.val_losses)
    np.save(mycnn.save_file + '_val_accuracy.npy', mycnn.history.val_accuracies)

    np.save((mycnn.save_file + '_noise.npy'),mycnn.CNN.layers[1].get_weights())
    noise = np.array(mycnn.CNN.layers[1].get_weights()).reshape((28,28))

    # Picking

    test_score = mycnn.CNN.evaluate(mycnn.x_test, mycnn.y_test, batch_size = 100, verbose = 0)
    print('Accuracy on test data: ' + str(test_score[1]))

    num_samples = 9
    rand_ind = [int(f) for f in 10000*np.random.random(num_samples)]
    data = mycnn.x_test[rand_ind]
    labels = np.argmax(mycnn.y_test_orig[rand_ind],axis = 1)

    fig = plt.figure()
    for i in range(num_samples):
        plt.subplot(3,3,i+1)
        plt.imshow(data[i].reshape((28,28)))
        plt.axis('off')
    plt.show()

    P = mycnn.CNN.predict(data, verbose = 0)
    pred = np.argmax(P,axis = 1)
    print('Ground Truth: ' + str(labels))
    print('Predictions: ' + str(pred))
    np.save(mycnn.save_file + '_prediction_gt.npy', labels)
    np.save(mycnn.save_file + '_prediction_data.npy', data)
    np.save(mycnn.save_file + '_prediction_y.npy', pred)

    for target in range(1,10):
        tempcnn = mnist_adversarial_CNN(target)
        tempcnn.train_no_history()

        np.save((tempcnn.save_file + '_noise.npy'),tempcnn.CNN.layers[1].get_weights())
        P = tempcnn.CNN.predict(data, verbose = 0)
        pred = np.argmax(P,axis = 1)
        print('Ground Truth: ' + str(labels))
        print('Predictions: ' + str(pred))
        np.save(tempcnn.save_file + '_prediction_gt.npy', labels)
        np.save(tempcnn.save_file + '_prediction_data.npy', data)
        np.save(tempcnn.save_file + '_prediction_y.npy', pred)
