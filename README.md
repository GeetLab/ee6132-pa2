# Programming Assignment: Exercises on MNIST

Classification and other tasks on MNIST data, using Keras library.

## Q1: Classification

Create multiple models and compare their performance. 

## Q2: Adverserial Examples

Freeze the weights of the trained models obtained from Q1.
Create a custom layer that simply adds noise to the input image. The noise is trainable.

By setting a target digit, training is done to produce an image which when added to any digit produces adverserial examples of that digit.

## Q3: Visualizing Filters

Freeze the weights of the trained models obtained from Q1. Make the input image itself be trainable, and start with random initialization of pixels.
Set the loss to be the output of the filters of interest and do gradient ASCENT.




