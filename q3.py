import numpy as np
import keras, sys, os
from keras import backend as K
from keras import callbacks, regularizers, optimizers, utils, losses
from keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense, Activation
from keras.layers.core import Reshape
from keras.models import Model
import matplotlib.pyplot as plt
import tensorflow as tf

from keras.engine.topology import Layer
from keras.legacy import interfaces

from mpl_toolkits.mplot3d import Axes3D

TARGET_CLASS = 0
MAXIM_LOGIT = False

def LogitLoss(yt, yp):
    return yp[:,TARGET_CLASS]

def pool2_loss(yt, yp):
    # 7,7,32, so individual mapping is 7,7; central neuron will be [3,3]
    return yp[:,3,3,TARGET_CLASS]

class my_SGD(optimizers.Optimizer):
    """
    Custom Optimizer based on SGD
    """

    def __init__(self, wt_decay = 0.0001, num_iter = 200, **kwargs):
        super(my_SGD, self).__init__(**kwargs)
        with K.name_scope(self.__class__.__name__):
            self.iterations = K.variable(0, dtype='int64', name='iterations')
            self.wt_decay = K.variable(wt_decay, name='wt_decay')
            self.num_iter = K.variable(num_iter, name='num_iter')
        self.current_iter = 0

    @interfaces.legacy_get_updates_support
    def get_updates(self, loss, params):
        grads = self.get_gradients(loss, params)
        self.updates = [K.update_add(self.iterations, 1)]

        # grads[0] /= (K.sqrt(K.mean(K.square(grads[0]))) + 1e-5)
        # grads[0] /= K.sqrt(K.mean(K.square(grads[0])))
        # mean = K.sqrt(K.mean(K.square(grads[0])))

        step_size = 1.0/K.std(K.flatten(grads[0]))
        inds = K.arange(0,28,step=1.0, dtype='float32')
        inds_2dx = K.repeat_elements(K.reshape(inds,(1,28,1)),28,0)
        inds_2dy = K.repeat_elements(K.reshape(inds,(28,1,1)),28,1)

        for p, g in zip(params, grads):
            # sigma = K.pow(self.current_iter*4.0/(self.num_iter + 0.5),2)
            # sigma = (self.current_iter*4.0 + 0.4)/(self.num_iter + 0.5)
            sigma = K.pow(0.5 + 4*self.current_iter/self.num_iter,2)
            # g_smooth = K.exp( K.pow(g/K.sqrt(sigma),2)/(2.0) )/(K.sqrt(sigma)*np.sqrt(2.0*np.pi))

            g_smooth_list = []
            g_ = g
            for r in range(28):
                for c in range(28):
                    kernel_2d = K.exp(-(K.pow(inds_2dx-c,2) + K.pow(inds_2dy-r,2)) /(2.0*sigma) )
                    kernel_2d = kernel_2d / (2.0 * np.pi * sigma)
                    g_smooth_list.append(K.sum(K.flatten(tf.multiply(kernel_2d,g_))))

            g_smooth = K.reshape(tf.stack(g_smooth_list),(28,28,1))
            # if self.current_iter < 3:
            #     new_p = (1.0 - self.wt_decay) * p + step_size * g
            # else:
            #     new_p = (1.0 - self.wt_decay) * p + step_size * g_smooth
            new_p = (1.0 - self.wt_decay) * p + step_size * g_smooth
            # new_p = (1.0 - self.wt_decay) * p + step_size * g
            # Apply constraints.
            if getattr(p, 'constraint', None) is not None:
                new_p = p.constraint(new_p)

            self.updates.append(K.update(p, new_p))
        self.current_iter += 1
        return self.updates

    def get_config(self):
        config = {'wt_decay': float(K.get_value(self.wt_decay)),
                  'num_iter': float(K.get_value(self.num_iter))}
        base_config = super(my_SGD, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

class AddNoise(Layer):
    def __init__(self, **kwargs):
        super(AddNoise, self).__init__(**kwargs)

    def build(self, input_shape):
        self.kernel = self.add_weight(name = 'kernel',
        shape = (input_shape[1],input_shape[2],input_shape[3]),
        initializer = keras.initializers.RandomNormal(mean = 128.0, stddev = 0.08),
        trainable = True)
        super(AddNoise, self).build(input_shape)

    def call(self, x):
        return tf.clip_by_value(tf.add(x, self.kernel),0,255)

    def compute_output_shape(self, input_shape):
        return input_shape

class TrainHistory(keras.callbacks.Callback):
    def __init__(self):
        self.train_losses = []

    def on_batch_end(self, batch, logs={}):
        self.train_losses.append(logs.get('loss'))


        # ensure noise is between 0 and 255
        # intermediate_layer_model1 = Model(inputs = self.model.input,outputs = self.model.layers[1].output)
        # intermediate_output1 = intermediate_layer_model1.predict(self.x_val[0].reshape((1,28,28,1)))
        # noise = self.model.layers[1].get_weights()
        # subtrahend = np.clip(noise,0,255)
        # self.model.layers[1].set_weights(np.clip(noise,0,255))


        # if self.count%100 == 0:
        #     print('Weights: ')
        #     print(np.array(self.model.layers[1].get_weights()).flatten()[0:100])
        #     print('Layer 0:')
        #     intermediate_layer_model = Model(inputs = self.model.input,outputs = self.model.layers[0].output)
        #     intermediate_output = intermediate_layer_model.predict(self.x_val[0].reshape((1,28,28,1)))
        #     print(np.array(intermediate_output).flatten()[0:100])
        #     print('Layer 1:')
        #     intermediate_layer_model1 = Model(inputs = self.model.input,outputs = self.model.layers[1].output)
        #     intermediate_output1 = intermediate_layer_model1.predict(self.x_val[0].reshape((1,28,28,1)))
        #     print(np.array(intermediate_output1).flatten()[0:100])
        #     print('Out:')
        #     intermediate_layer_model2 = Model(inputs = self.model.input,outputs = self.model.layers[10].output)
        #     intermediate_output2 = intermediate_layer_model2.predict(self.x_val[0].reshape((1,28,28,1)))
        #     print(np.array(intermediate_output2).flatten()[0:100])

            # if len(self.temp) == 0:
            #     self.temp.extend(self.model.layers[4].get_weights())
            # else:
            #     diff = np.array(self.temp) - np.array(self.model.layers[4].get_weights())
            #     self.temp = []
            #     self.temp.extend(self.model.layers[4].get_weights())
            #     print("Diff : " + str(np.sum(np.sum(diff.flatten()))))


            # noise = self.model.layers[1].get_weights()
            # print(np.clip(noise,0,255).flatten())

class mnist_adversarial_CNN:
    def __init__(self, target_class):
        self.BATCH_SIZE = 1
        self.INIT_STDDEV = 0.08
        self.WT_DECAY = 0.0001
        self.EPOCHS = 1
        self.tc = target_class

        self.LR = 0.7
        self.DECAY = 0.0001
        self.MOMENTUM = 0

        self.load_data()
        self.history = TrainHistory()
        if MAXIM_LOGIT:
            self.CNN = self.get_frozen_CNN_1()
        else:
            self.CNN = self.get_frozen_CNN_2()
        self.save_file = str.format('mnist_cnn_visual{0}',str(self.tc))

        # if os.path.exists(self.save_file + '.hdf5'):
        #     self.CNN.load_weights(self.save_file + '.hdf5')

    def load_data(self):
        self.x_train = np.zeros((200,784)).reshape((-1,28,28,1))
        if MAXIM_LOGIT:
            self.y_train = np.zeros((200,10))    # just dummies
        else:
            self.y_train = np.zeros((200,7,7,32))

        # self.y_train = utils.to_categorical(self.tc * np.ones(200),10)

    def get_conv(self, layer_obj):
        return Conv2D(32,(3,3), activation = 'relu', padding = 'same')(layer_obj)

    def get_frozen_conv(self, layer_obj):
        return Conv2D(32,(3,3), activation = 'relu', padding = 'same',
        trainable = False)(layer_obj)

    def get_dense(self, layer_obj, num_units):
        return Dense(num_units)(layer_obj)

    def get_frozen_dense(self, layer_obj, num_units):
        return Dense(num_units, trainable = False)(layer_obj)

    def get_pretrained_weights(self):
        inputs = Input(shape = (28,28,1))
        conv1 = self.get_conv(inputs)
        pool1 = MaxPooling2D(pool_size = (2,2))(conv1)
        conv2 = self.get_conv(pool1)
        pool2 = MaxPooling2D(pool_size = (2,2))(conv2)
        dense1 = self.get_dense(Flatten()(pool2),500)
        dense1 = Activation('relu')(dense1)
        out = self.get_dense(dense1,10)
        out = Activation('softmax')(out)
        model = Model(input = inputs, output = out)
        model.compile(optimizer = optimizers.SGD(lr = self.LR, momentum = self.MOMENTUM, decay = self.DECAY),
        loss = 'categorical_crossentropy', metrics = ['accuracy'])
        model.load_weights('mnist_cnn.hdf5')

        # layers : Input, Conv[1], Pool, Conv[3], Pool, Flatten, Dense[6], Relu, Dense[8], Softmax

        return {'conv1': model.layers[1].get_weights(),
        'conv2': model.layers[3].get_weights(),
        'dense1': model.layers[6].get_weights(),
        'out': model.layers[8].get_weights()}

    def get_frozen_CNN_1(self):
        pt_weights = self.get_pretrained_weights()

        inputs = Input(shape = (28,28,1))
        input_noise = AddNoise()(inputs)

        conv1 = self.get_frozen_conv(input_noise)
        pool1 = MaxPooling2D(pool_size = (2,2))(conv1)

        conv2 = self.get_frozen_conv(pool1)
        pool2 = MaxPooling2D(pool_size = (2,2))(conv2)

        dense1 = self.get_frozen_dense(Flatten()(pool2),500)
        dense1 = Activation('relu')(dense1)
        out = self.get_frozen_dense(dense1,10)


        model = Model(input = inputs, output = out)

        # in this case, layers : inputs, noise[1], conv[2], pool, conv[4], pool, flatten, dense[7], relu, dense[9]
        model.layers[2].set_weights(pt_weights['conv1'])
        model.layers[4].set_weights(pt_weights['conv2'])
        model.layers[7].set_weights(pt_weights['dense1'])
        model.layers[9].set_weights(pt_weights['out'])

        model.compile(loss = LogitLoss,
        optimizer = my_SGD(wt_decay = self.WT_DECAY, num_iter = 200))

        return model

    def get_frozen_CNN_2(self):
        pt_weights = self.get_pretrained_weights()

        inputs = Input(shape = (28,28,1))
        input_noise = AddNoise()(inputs)

        conv1 = self.get_frozen_conv(input_noise)
        pool1 = MaxPooling2D(pool_size = (2,2))(conv1)

        conv2 = self.get_frozen_conv(pool1)
        pool2 = MaxPooling2D(pool_size = (2,2))(conv2)
        out = MaxPooling2D(pool_size = (2,2))(conv2)

        model = Model(input = inputs, output = out)

        # in this case, layers : inputs, noise[1], conv[2], pool, conv[4], pool
        model.layers[2].set_weights(pt_weights['conv1'])
        model.layers[4].set_weights(pt_weights['conv2'])

        model.compile(loss = pool2_loss,
        optimizer = my_SGD(wt_decay = self.WT_DECAY, num_iter = 200))

        return model

    def train(self):
        self.CNN.fit(self.x_train, self.y_train, batch_size = self.BATCH_SIZE, epochs = self.EPOCHS, verbose = 1, shuffle = True, callbacks = [self.history])

    def train_no_history(self):
        self.CNN.fit(self.x_train, self.y_train, batch_size = self.BATCH_SIZE, epochs = self.EPOCHS, verbose = 1, shuffle = True)
        # self.CNN.save(self.save_file + '.hdf5')

if __name__ == '__main__':
    target = 0
    if len(sys.argv) > 1:
        target = int(sys.argv[1])
        TARGET_CLASS = target

    print('###### Layer number: ' + str(TARGET_CLASS))
    mycnn = mnist_adversarial_CNN(TARGET_CLASS)
    mycnn.train()

    print(mycnn.history.train_losses)
    # np.save(mycnn.save_file + '_train_losses.npy', mycnn.history.train_losses)

    noise = np.array(mycnn.CNN.layers[1].get_weights()).reshape((28,28))
    np.save(mycnn.save_file + "_noise.npy",noise)
    plt.imshow(noise)
    plt.show()
