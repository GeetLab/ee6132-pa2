import matplotlib.pyplot as plt
import numpy as np

sample = np.load('./results/q2/mnist_adv_cnn_0_prediction_data.npy').reshape((-1,28,28))
fig = plt.figure()
for i in range(9):
    plt.subplot(11,10,i+2)
    plt.imshow(sample[i])
    plt.axis('off')

for i in range(10):
    noise = np.load('./results/q2/mnist_adv_cnn_' + str(i) + '_noise.npy').reshape((28,28))
    plt.subplot(11,10,(i+1)*10 + 1)
    plt.imshow(noise)
    plt.axis('off')
    for j in range(9):
        plt.subplot(11,10,(i+1)*10 + j + 2)
        plt.imshow(np.clip(noise + sample[j],0,255))
        plt.axis('off')
plt.show()

for i in range(10):
    noise = np.load('./results/q2/mnist_adv_cnn_' + str(i) + '_noise.npy').reshape((28,28))
    plt.imshow(noise)
    plt.show()
