import matplotlib.pyplot as plt
import numpy as np

train_losses = np.load('./results/q1_3/train_losses.npy')
val_losses = np.load('./results/q1_3/val_losses.npy')
train_iters = range(len(train_losses))
# since test loss is evaluated only after every 200 iterations
val_iters = 200*np.array(range(len(val_losses)))

plt.plot(train_iters,train_losses,'go-',label='Training')
plt.plot(val_iters,val_losses,'ro-',label='Validation')
plt.ylabel('Loss')
plt.xlabel('Iteration')
plt.legend()
plt.show()
