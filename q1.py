import numpy as np
import keras, sys, os
from keras import backend as K
from keras import callbacks, regularizers, optimizers, utils
from keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense, Activation
from keras.layers.core import Reshape
from keras.models import Model
from sklearn.datasets import fetch_mldata
import matplotlib.pyplot as plt

class TrainHistory(keras.callbacks.Callback):
    def __init__(self, X_val, Y_val):
        self.x_val = X_val
        self.y_val = Y_val
        self.train_losses = []
        self.train_accuracies = []
        self.val_losses = []
        self.val_accuracies = []
        self.count = 0

    def on_batch_end(self, batch, logs={}):
        self.train_losses.append(logs.get('loss'))
        self.train_accuracies.append(logs.get('acc'))
        if self.count%200 == 0:
            val_score = self.model.evaluate(self.x_val, self.y_val, batch_size = 100, verbose = 0)
            self.val_losses.append(val_score[0])
            self.val_accuracies.append(val_score[1])
        self.count += 1

    def on_train_end(self, logs={}):
        val_score = self.model.evaluate(self.x_val, self.y_val, batch_size = 100, verbose = 0)
        self.val_losses.append(val_score[0])
        self.val_accuracies.append(val_score[1])

class mnist_CNN:
    def __init__(self,arch = 0):
        self.BATCH_SIZE = 100
        self.INIT_STDDEV = 0.08
        self.REGULARIZATION = 0.005
        self.LR = 0.001
        self.DECAY = 0.0001
        self.EPOCHS = 10
        self.MOMENTUM = 0.7
        self.ARCH = arch

        self.load_data()
        self.CNN = self.get_CNN()
        self.history = TrainHistory(self.x_val, self.y_val)
        if os.path.exists('mnist_cnn.hdf5'):
            self.CNN.load_weights('mnist_cnn.hdf5')

    def load_data(self):
        mnist = fetch_mldata('MNIST original', data_home='./Data/')
        rand_ind = np.random.permutation(range(70000))
        data = mnist.data[rand_ind]
        labels = mnist.target[rand_ind]

        self.x_train = mnist.data[rand_ind[0:50000]].reshape((-1,28,28,1))
        self.y_train = utils.to_categorical(mnist.target[rand_ind[0:50000]], 10)
        self.x_val = mnist.data[rand_ind[50000:60000]].reshape((-1,28,28,1))
        self.y_val = utils.to_categorical(mnist.target[rand_ind[50000:60000]], 10)
        self.x_test = mnist.data[rand_ind[60000:70000]].reshape((-1,28,28,1))
        self.y_test = utils.to_categorical(mnist.target[rand_ind[60000:70000]], 10)

    def get_conv(self, layer_obj):
        return Conv2D(32,(3,3), activation = 'relu', padding = 'same',
        kernel_initializer = 'glorot_normal',
        bias_initializer = keras.initializers.RandomNormal(mean = 1.0, stddev=self.INIT_STDDEV),
        kernel_regularizer = regularizers.l2(self.REGULARIZATION))(layer_obj)

    def get_dense(self, layer_obj, num_units):
        return Dense(num_units,
        kernel_initializer = 'glorot_normal',
        bias_initializer = keras.initializers.RandomNormal(mean = 1.0, stddev=self.INIT_STDDEV),
        kernel_regularizer = regularizers.l2(self.REGULARIZATION))(layer_obj)

    def get_CNN(self):
        if self.ARCH == 0:
            inputs = Input(shape = (28,28,1))
            conv1 = self.get_conv(inputs)
            pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
            out = self.get_dense(Flatten()(pool1),10)
            out = Activation('softmax')(out)
            model = Model(input = inputs, output = out)
            model.compile(optimizer = optimizers.SGD(lr = self.LR, momentum = self.MOMENTUM, decay = self.DECAY),
            loss = 'categorical_crossentropy', metrics = ['accuracy'])
            return model
        if self.ARCH == 1:
            inputs = Input(shape = (28,28,1))
            conv1 = self.get_conv(inputs)
            pool1 = MaxPooling2D(pool_size = (2,2))(conv1)
            conv2 = self.get_conv(pool1)
            pool2 = MaxPooling2D(pool_size = (2,2))(conv2)
            out = self.get_dense(Flatten()(pool2),10)
            out = Activation('softmax')(out)
            model = Model(input = inputs, output = out)
            model.compile(optimizer = optimizers.SGD(lr = self.LR, momentum = self.MOMENTUM, decay = self.DECAY),
            loss = 'categorical_crossentropy', metrics = ['accuracy'])
            return model
        if self.ARCH == 2:
            inputs = Input(shape = (28,28,1))
            conv1 = self.get_conv(inputs)
            pool1 = MaxPooling2D(pool_size = (2,2))(conv1)
            conv2 = self.get_conv(pool1)
            pool2 = MaxPooling2D(pool_size = (2,2))(conv2)
            dense1 = self.get_dense(Flatten()(pool2),500)
            dense1 = Activation('relu')(dense1)
            out = self.get_dense(dense1,10)
            out = Activation('softmax')(out)
            model = Model(input = inputs, output = out)
            model.compile(optimizer = optimizers.SGD(lr = self.LR, momentum = self.MOMENTUM, decay = self.DECAY),
            loss = 'categorical_crossentropy', metrics = ['accuracy'])
            return model

    def train(self):
        hist = self.CNN.fit(self.x_train, self.y_train, batch_size = self.BATCH_SIZE, epochs = self.EPOCHS, verbose = 1, shuffle = True,
        callbacks = [self.history])
        self.CNN.save('mnist_cnn.hdf5')


if __name__ == '__main__':
    arch = 0
    if len(sys.argv) > 1:
        arch = int(sys.argv[1])
    mycnn = mnist_CNN(arch)
    mycnn.train()


    np.save('train_losses.npy', mycnn.history.train_losses)
    np.save('train_accuracy.npy', mycnn.history.train_accuracies)
    np.save('val_losses.npy', mycnn.history.val_losses)
    np.save('val_accuracy.npy', mycnn.history.val_accuracies)

    # Picking
    print('Architecture : ' + str(arch))
    test_score = mycnn.CNN.evaluate(mycnn.x_test, mycnn.y_test, batch_size = 100, verbose = 0)
    print('Accuracy on test data: ' + str(test_score[1]))


    rand_ind = [int(f) for f in 10000*np.random.random(20)]
    data = mycnn.x_test[rand_ind]
    labels = np.argmax(mycnn.y_test[rand_ind],axis = 1)

    fig = plt.figure()
    for i in range(20):
        plt.subplot(4,5,i+1)
        plt.imshow(data[i].reshape((28,28)))
        plt.axis('off')
    plt.show()

    # Adding noises: Checking adversarial examples
    # noise = np.load('noise.npy').reshape((784,))
    # temp = np.repeat(noise,20,axis = 0).reshape((784,20))
    # noise_all = np.transpose(temp).reshape((-1,28,28,1))
    # print(noise_all.shape)
    #
    # data = data + noise_all


    P = mycnn.CNN.predict(data, verbose = 0)
    pred = np.argmax(P,axis = 1)
    print('Ground Truth: ' + str(labels))
    print('Predictions: ' + str(pred))
    np.save('prediction_gt.npy', labels)
    np.save('prediction_data.npy', data)
    np.save('prediction_y.npy', pred)
